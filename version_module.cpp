#include <pybind11/embed.h>

#include "version.hpp"

namespace version {

void bind_version(pybind11::module& m){
    m.def("get_compilation_config", &get_compilation_config)
     .def("get_version",            &get_version);
}

} //namespace version

PYBIND11_EMBEDDED_MODULE(version_module, m){
    version::bind_version(m);
}
